#include <stdio.h>
int palindrome(char *s);
int quantiteChars(char *s);
int main() {
    char chaine[80];
    printf("Entrez une chaîne: ");
    fgets(chaine, 80, stdin);
    fputs(chaine, stdout);
    char *s = chaine;
    int a = quantiteChars(s);
    printf("Les nombres de caracteres est : %i\n", a);
    int p = palindrome(s);
    printf("La phrase est Palindrome: %i\n",p);
}

int quantiteChars(char *s){
    int a = 0;
    char b = *s;
    while(b != '\n'){
      a++;
      s++;
      b = *s;
    }
   return a;

}

int palindrome(char *s){
   int max = quantiteChars(s);
   char *sf = s+max-1;
   for(int i = 0; i < max; i++){
      char a = *(s+i);
      char b = *(sf-i);
      if(b != a){
         return 0;
      }
   }
  return 1;
}
