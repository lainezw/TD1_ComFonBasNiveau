#include <stdio.h>
int main() {
    int t[2];
    printf("Deux ints et ses adresse\n");
    printf("Premiere adresse: %lu,\nseconde adresse: %lu\n",
    (long unsigned) t,
    (long unsigned) (t+1));
    printf("\ndeux floats et ses adresse\n");
    float tf[2];
    printf("Premiere adresse: %lu,\nSeconde adresse: %lu\n",
    (long unsigned) tf,
    (long unsigned) (tf+1));
    double td[2];
    printf("\nDeux double et ses adresse\n");
    printf("Premiere adresse: %lu,\nseconde adresse: %lu\n",
    (long unsigned) td,
    (long unsigned) (td+1));

    char tc[2];
    printf("\nDeux char et ses adresse\n");
    printf("Premiere adresse: %lu,\nseconde adresse: %lu\n",
    (long unsigned) tc,
    (long unsigned) (tc+1));

    int tint[4] = {1,2,3,4};

    printf("\nUn for pour un tab de ints avec la syntax");
    for(int i = 0; i < 4; i++){
       printf("\nAdresse %i : %lu", i , (long unsigned) (tint+i));
    }
    printf("\n");
    int a, *b, c;
    a = 12;
    b = &a;
    printf("\na veut : %i \nb (pointer) est : %lu \n\n", a, (long unsigned) b);
    c = 15;
    printf("\nMaintenant On a c\na veut : %i \nb (pointer) est : %lu\nOn a C qui veut : %i\n\n", a, (long unsigned) b, c);
    *b = c;
    printf("\nMaintenant en b on a mit c\na veut : %i \nb (pointer) est : %lu\nEt C veut : %i \n", a, (long unsigned) b,c);
}
