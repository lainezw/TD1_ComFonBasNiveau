#include <stdio.h>
#define MESSAGE "Super"
int dbl(int a);

int main() {
    printf("Youpi !\n");
    printf(MESSAGE);
    int a = dbl(4);
    printf("\nLe double de 4 est :  %i\n\n",a);
    int tab[4] = {1,4,8,0};
    tab[3] = tab[1] + tab[2];
    for(int i = 0; i < 4; i++){
       printf("%i ",tab[i]);
    }
   printf("\n");
}

int mult(int a, int b) {
    return a*b;
}

int dbl(int a){
   return a*2;
}
